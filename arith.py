"""
Name : arith.py
Function : Addition, Subtraction, Multiplication, Division, Modulus
Purpose : Adding Arithmetic Functions for reuse

"""


# Product Function :

def prod(a,b):
    print(a*b)

# Division Function :    

def divide(a,b):
    print(a/b)

# Modulus Function :

def mod(a,b):
    print(a%b)

# Addition Function :

def add(a,b):
    print(a+b)

# Subtraction Function :

def minus(a,b):
    print(a-b)



